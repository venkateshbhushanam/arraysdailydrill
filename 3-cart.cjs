const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        }, {
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]


/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/


let allItemsPriceGreaterthan65 =Object.entries(...products).reduce((itemArray, eachItem) => {
 
        if (Object.keys(eachItem[1]).includes('price')) {
            itemArray.push(eachItem)
        }else {
            eachItem[1].forEach(each => {
                itemArray.push(...Object.entries(each))
            })
            
        }
  

    return itemArray

}, [])
    .filter(eachProduct => {
        if (parseInt(eachProduct[1].price.replace("$", "")) > 65) {
            return true

        }
    })

console.log(allItemsPriceGreaterthan65)


let allItemsOrdermorethan1 = Object.entries(...products).reduce((itemArray, eachItem) => {
   
        if (Object.keys(eachItem[1]).includes('price')) {
            itemArray.push(eachItem)
        }else {
            eachItem[1].forEach(each => {
                itemArray.push(...Object.entries(each))
            })
        }
  

    return itemArray

}, [])
    .filter(eachProduct => {
        if (parseInt(eachProduct[1].quantity) > 1) {
            return true

        }
    })

//console.log(allItemsOrdermorethan1)


let fragileItems = Object.entries(...products).reduce((itemArray, eachItem) => {
    
        if (Object.keys(eachItem[1]).includes('price')) {
            itemArray.push(eachItem)
        }else {
            eachItem[1].forEach(each => {
                itemArray.push(...Object.entries(each))
            })
        }
    
    return itemArray

}, [])
    .filter(eachProduct => {
        if (eachProduct[1].type=='fragile') {
            return true
        }
    })

//console.log(fragileItems)


let leastAndMostExpensive = Object.entries(...products).reduce((itemArray, eachItem) => {
    
        if (Object.keys(eachItem[1]).includes('price')) {
            itemArray.push(eachItem)
        }else {
            eachItem[1].forEach(each => {
                itemArray.push(...Object.entries(each))
            })
        }
    
    return itemArray

}, [])
    .filter(eachProduct => {
        if (parseInt(eachProduct[1].quantity) ==1) {
            return true

        }
    })
    .sort(((item1,item2)=>{
        return Number(item1[1].price.replace("$",""))-Number(item2[1].price.replace("$",""))
    }))

let itemsList=[leastAndMostExpensive[0],leastAndMostExpensive[leastAndMostExpensive.length-1]]

//console.log(Object.fromEntries(itemsList))