const favouritesMovies = {
  Matrix: {
    imdbRating: 8.3,
    actors: ["Keanu Reeves", "Carrie-Anniee"],
    oscarNominations: 2,
    genre: ["sci-fi", "adventure"],
    totalEarnings: "$680M",
  },
  FightClub: {
    imdbRating: 8.8,
    actors: ["Edward Norton", "Brad Pitt"],
    oscarNominations: 6,
    genre: ["thriller", "drama"],
    totalEarnings: "$350M",
  },
  Inception: {
    imdbRating: 8.3,
    actors: ["Tom Hardy", "Leonardo Dicaprio"],
    oscarNominations: 12,
    genre: ["sci-fi", "adventure"],
    totalEarnings: "$870M",
  },
  "The Dark Knight": {
    imdbRating: 8.9,
    actors: ["Christian Bale", "Heath Ledger"],
    oscarNominations: 12,
    genre: ["thriller"],
    totalEarnings: "$744M",
  },
  "Pulp Fiction": {
    imdbRating: 8.3,
    actors: ["Sameul L. Jackson", "Bruce Willis"],
    oscarNominations: 7,
    genre: ["drama", "crime"],
    totalEarnings: "$455M",
  },
  Titanic: {
    imdbRating: 8.3,
    actors: ["Leonardo Dicaprio", "Kate Winslet"],
    oscarNominations: 13,
    genre: ["drama"],
    totalEarnings: "$800M",
  },
};

/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.
    NOTE: Do not change the name of this file
*/

//Q1. Find all the movies with total earnings more than $500M.

let moreThen500MMoviesList = Object.entries(favouritesMovies).filter(
  (each) => parseInt(each[1].totalEarnings.replace("$", "")) > 500
);

console.log(moreThen500MMoviesList);

//Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.

let movieWithOscarNominationsAndTotalEarningsGreatethan500 = Object.entries(
  favouritesMovies
).filter(
  (each) =>
    parseInt(each[1].totalEarnings.replace("$", "")) > 500 &&
    parseInt(each[1].oscarNominations) > 3
);

console.log(movieWithOscarNominationsAndTotalEarningsGreatethan500);

//Q.3 Find all movies of the actor "Leonardo Dicaprio".

let moviesLeonardoDicaprio = Object.entries(favouritesMovies).filter((each) =>
  each[1].actors.includes("Leonardo Dicaprio")
);

console.log(moviesLeonardoDicaprio);

//Q.4 Sort movies (based on IMDB rating)
//if IMDB ratings are same, compare totalEarning as the secondary metric.

let sortedMoviesListBasedOnIMDB = Object.fromEntries(
  Object.entries(favouritesMovies).sort((movie1, movie2) => {
    if (movie1[1].imdbRating == movie2[1].imdbRating) {
      return (
        parseInt(movie1[1].totalEarnings.replace("$", "")) -
        parseInt(movie2[1].totalEarnings.replace("$", ""))
      );
    }
    return movie1[1].imdbRating - movie2[1].imdbRating;
  })
);

console.log(sortedMoviesListBasedOnIMDB);

//Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
//drama > sci-fi > adventure > thriller > crime

