const items = [
  {
    name: "Orange",
    available: true,
    contains: "Vitamin C",
  },
  {
    name: "Mango",
    available: true,
    contains: "Vitamin K, Vitamin C",
  },
  {
    name: "Pineapple",
    available: true,
    contains: "Vitamin A",
  },
  {
    name: "Raspberry",
    available: false,
    contains: "Vitamin B, Vitamin A",
  },
  {
    name: "Grapes",
    contains: "Vitamin D",
    available: false,
  },
];

//NOTE: Do not change the name of this file

// 1. Get all items that are available

let availableItems = items.filter((eachObj) => eachObj.available);
console.log(availableItems);

//2. Get all items containing only Vitamin C.

let VitaminCItems = items.filter((eachObj) => {
  let vitamin = eachObj.contains.split(",");
  return vitamin.length == 1 && vitamin[0] == "Vitamin C";
});
console.log(VitaminCItems);

//3. Get all items containing Vitamin A.

let VitaminAItems = items.filter((eachObj) =>
  eachObj.contains.includes("Vitamin A")
);

console.log(VitaminAItems);

/*4. Group items based on the Vitamins that they contain in the following format:
    {
        "Vitamin C": ["Orange", "Mango"],
        "Vitamin K": ["Mango"],
    }
        
    and so on for all items and all Vitamins.*/

let vitaminGroup = items.reduce((vitaminObj, eachObj) => {
  eachObj.contains.split(", ").map((each) => {
    if (vitaminObj[each]) {
      vitaminObj[each].push(eachObj.name);
    } else {
      vitaminObj[each] = [];
      vitaminObj[each].push(eachObj.name);
    }
  });

  return vitaminObj;
}, {});

console.log(vitaminGroup);

//5. Sort items based on number of Vitamins they contain.

let sortedItemsOnNoOfVitamins = items.sort((obj1, obj2) => {
  if (obj1.contains.split(", ").length < obj2.contains.split(", ").length) {
    return -1;
  }
  if (obj1.contains.split(", ").length > obj2.contains.split(", ").length) {
    return 1;
  }
  return 0;
});
console.log(sortedItemsOnNoOfVitamins);
