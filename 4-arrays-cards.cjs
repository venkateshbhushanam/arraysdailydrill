let cards = [
  { id: 1, card_number: "5602221055053843723", card_type: "china-unionpay", issue_date: "5/25/2021", salt: "x6ZHoS0t9vIU", phone: "339-555-5239" },
  { id: 2, card_number: "3547469136425635", card_type: "jcb", issue_date: "12/18/2021", salt: "FVOUIk", phone: "847-313-1289" },
  { id: 3, card_number: "5610480363247475108", card_type: "china-unionpay", issue_date: "5/7/2021", salt: "jBQThr", phone: "348-326-7873" },
  { id: 4, card_number: "374283660946674", card_type: "americanexpress", issue_date: "1/13/2021", salt: "n25JXsxzYr", phone: "599-331-8099" },
  { id: 5, card_number: "67090853951061268", card_type: "laser", issue_date: "3/18/2021", salt: "Yy5rjSJw", phone: "850-191-9906" },
  { id: 6, card_number: "560221984712769463", card_type: "china-unionpay", issue_date: "6/29/2021", salt: "VyyrJbUhV60", phone: "683-417-5044" },
  { id: 7, card_number: "3589433562357794", card_type: "jcb", issue_date: "11/16/2021", salt: "9M3zon", phone: "634-798-7829" },
  { id: 8, card_number: "5602255897698404", card_type: "china-unionpay", issue_date: "1/1/2021", salt: "YIMQMW", phone: "228-796-2347" },
  { id: 9, card_number: "3534352248361143", card_type: "jcb", issue_date: "4/28/2021", salt: "zj8NhPuUe4I", phone: "228-796-2347" },
  { id: 10, card_number: "4026933464803521", card_type: "visa-electron", issue_date: "10/1/2021", salt: "cAsGiHMFTPU", phone: "372-887-5974" },
];

/* 

    2. Find all cards that were issued before June.
    
    4. Add a new field to each card to indicate if the card is valid or not.
    5. Invalidate all cards issued before March.
    6. Sort the data into ascending order of issue date.
    7. Group the data in such a way that we can identify which cards were assigned in which months.

    Use method chaining to solve #3, #4, #5, #6 and #7.

    NOTE: Do not change the name of this file 
*/

//1. Find all card numbers whose sum of all the even position digits is odd.

let allCardsNumSumOfEvenPostionOdd = cards.reduce((resArray, eachCard) => {
  let cardNumber = eachCard.card_number.split("");
  let total = 0;

  let evenSum = cardNumber.map((each, index) => {
    if (index % 2 == 0) {
      total += parseInt(each);
    }
  });
  if (total % 2 != 0) {
    resArray.push(eachCard.card_number);
  }

  return resArray;
}, []);

console.log(allCardsNumSumOfEvenPostionOdd);

//2. Find all cards that were issued before June.

let allCadsissuedBeforeJune = cards.filter((eachCard) => {
  let date = new Date(eachCard.issue_date);

  if (date.getMonth() + 1 < 6) {
    return true;
  }
});

console.log(allCadsissuedBeforeJune)

//3. Assign a new field to each card for CVV where the CVV is a random 3 digit number.

let cardsWithCvvField = cards.map((eachCard) => {
  let cvv = Math.ceil(Math.random() * 10000)

    .toString()
    .substring(0, 3);
  return { ...eachCard, CVV: +cvv };
});
//console.log(cardsWithCvvField)

//4. Add a new field to each card to indicate if the card is valid or not.

let cardsWithValidField = cards.map((eachCard) => {
  return { ...eachCard, validity: "" };
});

//console.log(cardsWithValidField)

//5. Invalidate all cards issued before March.

let cardsWithValidity = cards.map((eachCard) => {
  let issuedDate = eachCard.issue_date;
  let issuedMonth = new Date(issuedDate).getMonth() + 1;

  if (issuedMonth < 3) {
    return { ...eachCard, validity: "Invalid" };
  } else {
    return { ...eachCard, validity: "valid" };
  }
});

//console.log(carsWithValidity)

// 6. Sort the data into ascending order of issue date.

let sortedCardsOnIssuedDate = cards.sort((card1, card2) => {
  return new Date(card1.issue_date) - new Date(card2.issue_date);
});

//console.log(sortedCardsOnIssuedDate)

//7. Group the data in such a way that we can identify which cards were assigned in which months.

let groupedData = cards.reduce((monthsObj, eachCard) => {
  let dateOfEachCard = new Date(eachCard.issue_date).getMonth() + 1;

  if (monthsObj[dateOfEachCard]) {
    monthsObj[dateOfEachCard].push(eachCard);
  } else {
    monthsObj[dateOfEachCard] = [];
    monthsObj[dateOfEachCard].push(eachCard);
  }

  return monthsObj;
}, {});

//console.log(groupedData);




function chainedData() {
  let chainedData3_7 = cards
    .map((eachCard) => {
      let cvv = Math.ceil(Math.random() * 10000)

        .toString()
        .substring(0, 3);
      return { ...eachCard, CVV: +cvv };
    })

    .map((eachCard) => {
      return { ...eachCard, validity: "" };
    })

    .map((eachCard) => {
      let issuedDate = eachCard.issue_date;
      let issuedMonth = new Date(issuedDate).getMonth() + 1;

      if (issuedMonth < 3) {
        return { ...eachCard, validity: "Invalid" };
      } else {
        return { ...eachCard, validity: "valid" };
      }
    })

    .sort((card1, card2) => {
      return new Date(card1.issue_date) - new Date(card2.issue_date);
    })

    .reduce((monthsObj, eachCard) => {
      let dateOfEachCard = new Date(eachCard.issue_date).getMonth() + 1;

      if (monthsObj[dateOfEachCard]) {
        monthsObj[dateOfEachCard].push(eachCard);
      } else {
        monthsObj[dateOfEachCard] = [];
        monthsObj[dateOfEachCard].push(eachCard);
      }

      return monthsObj;
    }, {});
    console.log(chainedData3_7)
}
chainedData();
