let personsArray = [
  {
    id: 1,
    first_name: "Valera",
    last_name: "Pinsent",
    email: "vpinsent0@google.co.jp",
    gender: "Male",
    ip_address: "253.171.63.171",
  },
  {
    id: 2,
    first_name: "Kenneth",
    last_name: "Hinemoor",
    email: "khinemoor1@yellowbook.com",
    gender: "Polygender",
    ip_address: "50.231.58.150",
  },
  {
    id: 3,
    first_name: "Roman",
    last_name: "Sedcole",
    email: "rsedcole2@addtoany.com",
    gender: "Genderqueer",
    ip_address: "236.52.184.83",
  },
  {
    id: 4,
    first_name: "Lind",
    last_name: "Ladyman",
    email: "lladyman3@wordpress.org",
    gender: "Male",
    ip_address: "118.12.213.144",
  },
  {
    id: 5,
    first_name: "Jocelyne",
    last_name: "Casse",
    email: "jcasse4@ehow.com",
    gender: "Agender",
    ip_address: "176.202.254.113",
  },
  {
    id: 6,
    first_name: "Stafford",
    last_name: "Dandy",
    email: "sdandy5@exblog.jp",
    gender: "Female",
    ip_address: "111.139.161.143",
  },
  {
    id: 7,
    first_name: "Jeramey",
    last_name: "Sweetsur",
    email: "jsweetsur6@youtube.com",
    gender: "Genderqueer",
    ip_address: "196.247.246.106",
  },
  {
    id: 8,
    first_name: "Anna-diane",
    last_name: "Wingar",
    email: "awingar7@auda.org.au",
    gender: "Agender",
    ip_address: "148.229.65.98",
  },
  {
    id: 9,
    first_name: "Cherianne",
    last_name: "Rantoul",
    email: "crantoul8@craigslist.org",
    gender: "Genderfluid",
    ip_address: "141.40.134.234",
  },
  {
    id: 10,
    first_name: "Nico",
    last_name: "Dunstall",
    email: "ndunstall9@technorati.com",
    gender: "Female",
    ip_address: "37.12.213.144",
  },
];

// 1. Find all people who are Agender

const agenderData = personsArray.filter(
  (eachItem) => eachItem.gender == "Agender"
);
console.log(agenderData);

//2. Split their IP address into their components eg. 111.139.161.143
// has components [111, 139, 161, 143]

const spliittedIpComponents = personsArray.map((eachItem) => {
  let splittedIP = eachItem.ip_address.split(".").map((eachItem) => +eachItem);

  return { ...eachItem, ip_address: splittedIP };
});

console.log(spliittedIpComponents);

// 3. Find the sum of all the second components of the ip addresses.

const sumofSecondCompIp = personsArray.reduce((sumOfIP, eachItem) => {
  let splittedIP = eachItem.ip_address.split(".").map((eachItem) => +eachItem);
  sumOfIP = sumOfIP + splittedIP[1];
  return sumOfIP;
}, 0);
console.log(sumofSecondCompIp);

//3.(ii) Find the sum of all the fourth components of the ip addresses.
const sumofFourthCompIp = personsArray.reduce((sumOfIP, eachItem) => {
  let splittedIP = eachItem.ip_address.split(".").map((eachItem) => +eachItem);
  sumOfIP += splittedIP[splittedIP.length - 1];
  return sumOfIP;
}, 0);
console.log(sumofFourthCompIp);

//4. Compute the full name of each person and store it in a
//new key (full_name or something) for each person.

const personsFullNames = personsArray.reduce((fullNameArray, eachItem) => {
  let fullNameOfPerson = `${eachItem.first_name} ${eachItem.last_name}`;
  eachItem.full_name = fullNameOfPerson

  fullNameArray.push({ fullName: fullNameOfPerson });

  return fullNameArray;
}, []);

console.log(personsFullNames);

// 5. Filter out all the .org emails

let allOrgEmails = personsArray
  .filter((eachItem) => eachItem.email.includes("org"))
  .reduce((emailsList, eachItem) => {
    {
      emailsList.push(eachItem.email);
      return emailsList;
    }
  }, []);

console.log(allOrgEmails);

//6. Calculate how many .org, .au, .com emails are there
const emailsCalculations = personsArray.reduce(
  (emailsObj, eachItem) => {
    if (eachItem.email.endsWith(".org")) {
      emailsObj[".org"] += 1;
    } else if (eachItem.email.endsWith(".au")) {
      emailsObj[".au"] += 1;
    } else if (eachItem.email.endsWith(".com")) {
      emailsObj[".com"] += 1;
    }

    return emailsObj;
  },
  { ".org": 0, ".au": 0, ".com": 0 }
);

console.log(emailsCalculations);

//   7. Sort the data in descending order of first name

function compareObj(obj1, obj2) {
  if (obj1.first_name < obj2.first_name) {
    return 1;
  }
  if (obj1.first_name > obj2.first_name) {
    return -1;
  }
  return 0;
}

console.log(personsArray.sort(compareObj));
